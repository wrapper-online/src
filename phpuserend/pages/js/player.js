var json;
var videolist = document.getElementById('videolist');
var head = document.getElementsByTagName('head')[0];
var loadMore = document.getElementById('load_more');
var playerDiv = document.getElementById('player');
const listReq = new XMLHttpRequest();
listReq.open('GET', '/movieList');
listReq.send();
var C = 0;
        function getParameterByName(name, url = window.location.href) {
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
        var movid = getParameterByName('movieId');
function loadRows() {
    let c = C;
    C += 69;
    for (; c < C; c++) {
        if (c > json.length - 1) {
            loadMore.remove();
            break;
        }

        const tbl = json[c];
        const fin = json[c];
        const originalTitle = tbl.title;
        const title = originalTitle.replace(/(<([^>]+)>)/gi, "");
        const originalDesc = tbl.desc;
        const desc = originalDesc.replace(/(<([^>]+)>)/gi, "");
        const tblLongDesc = tbl.longDesc
        let finalLongDesc = tblLongDesc.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "")
        if (finalLongDesc === null) let finalLongDesc = 'No description provided.'
        if (tbl.id !== movid) {
            videolist.insertAdjacentHTML('beforeend', 
                '<div id="video" class="' + tbl.id + '"><div id="thumbnail"><img src="/movie_thumbs/' + tbl.id + '.png" style="width: 100%;"></div><div id="title">' + tbl.title.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</div><div id="description">' + tbl.longDesc.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</div><div id="duration">' + tbl.durationString + '</div></div>');
        } else {
            head.insertAdjacentHTML('beforeend',
                '<title>' + tbl.title.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</title><meta name="description" content="' + tbl.longDesc.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '"><meta content="' + tbl.longDesc.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '" property="og:description"><meta content="' + tbl.title.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '" property="og:title"><meta content="#5627d8" data-react-helmet="true" name="theme-color"><meta content="https://wrapper.online/player?movieId=' + tbl.id + '" property="og:url"><meta content="https://wrapper.online/movie_thumbs/' + tbl.id + '" property="og:image"><meta name="keywords" content="' + tbl.tags + '">');
            playerDiv.insertAdjacentHTML('beforeend',
                '<h3 id="videotitle">' + tbl.title.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</h3><p id="videodescription">' + finalLongDesc + '</p>');
        }
    }
}
videolist.onload = loadRows;
listReq.onreadystatechange = function(e) {
    if (listReq.readyState != 4) return;
    json = JSON.parse(listReq.responseText);
    loadRows();
}

function popup(id) {
    window.location.href = '/player?movieId=' + id;
}