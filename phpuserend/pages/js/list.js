var json;
var tbody = document.getElementsByTagName('tbody')[0];
var loadMore = document.getElementById('load_more');

const listReq = new XMLHttpRequest();
listReq.open('GET', 'http://localhost:8080/movieList');
listReq.send();

var C = 0;
function loadRows() {
    let c = C; C += 69;
    for (; c < C; c++) {
        if (c > json.length - 1) {
            loadMore.remove();
            break;
        }

        const tbl = json[c];
        const fin = json[c];
        const date = tbl.date.substr(0, 10) + ' ' + tbl.date.substr(11);
        function getParameterByName(name, url = window.location.href) {
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
        var debug = getParameterByName('debug');
		if (debug == 1) {
			tbody.insertAdjacentHTML('beforeend',
				'<tr><td><img src="http://localhost:8080/movie_thumbs/' + tbl.id + '.png"></td><td><div id="title"><xmp>' + tbl.title.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</xmp></div><div id="description"><xmp>' + tbl.desc.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</xmp></div><div><xmp>' + tbl.id + '</xmp></div><div><xmp>' + tbl.durationString + '</xmp></div></div></td><td><span>' + date.match(/./g).join('</span><span>') + '</span></td><td><a href="javascript:;" onclick="popup(\'' + tbl.id + '\')"></a><a href="/videomaker?movieId=' + tbl.id + '"></a><a href="/movies/' + tbl.id + '.xml" download="' + tbl.id + '"></a></td></tr>');
        } else {
			tbody.insertAdjacentHTML('beforeend',
                '<tr><td><img src="http://localhost:8080/movie_thumbs/' + tbl.id + '.png"></td><td><div id="title"><xmp>' + tbl.title.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</xmp></div><div id="description"><xmp>' + tbl.desc.replace(/&/g, "and").replace(/</g, "").replace(/>/g, "") + '</xmp></div><div><xmp>' + tbl.durationString + '</xmp></div></div></td><td><span>' + date.match(/./g).join('</span><span>') + '</span></td><td><a href="javascript:;" onclick="popup(\'' + tbl.id + '\')"></a><a href="/videomaker?movieId=' + tbl.id + '"></a><a href="/movies/' + tbl.id + '.xml" download="' + tbl.id + '"></a></td></tr>');
        }
    }
}

loadMore.onclick = loadRows;
listReq.onreadystatechange = function (e) {
    if (listReq.readyState != 4) return;
    json = JSON.parse(listReq.responseText);
    loadRows();
}

function popup(id) {
    window.location.href = '/player?movieId=' + id;
}
